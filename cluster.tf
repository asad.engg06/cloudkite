provider "aws" { region = "us-east-1" }
data "aws_region" "current" {}
module "vpc" {
  source   = "./modules/vpc"
  vpc_name = "EKS-VPC"
  additional_tags = {
    "kubernetes.io/cluster/${local.cluster_name}" = "owned",
    "kubernetes.io/role/intenral-elb"             = "1"
  }
}

resource "aws_iam_role" "eks-role" {
  name               = "eks-cluster-dev"
  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "dev-AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.eks-role.name
}

data "aws_eks_cluster" "eks" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "eks" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.eks.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.eks.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.eks.token
}

locals {
  vpc_id                    = module.vpc.vpc-id
  region                    = "us-east-1"
  cluster_name              = "dev-eks"
  subnets                   = ["${module.vpc.pvt-subnet-1}", "${module.vpc.pvt-subnet-1}"]
  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  asg_desired_capacity      = 3
  asg_max_size              = 4
  asg_min_size              = 2
  instance_type             = "t3.medium"
  origin_id = "node-origin"
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_version = "1.21"
  cluster_name    = local.cluster_name
  subnets         = ["${module.vpc.pvt-subnet-1}", "${module.vpc.pvt-subnet-2}"]
  vpc_id          = module.vpc.vpc-id
  enable_irsa     = true
  map_users = [
    {
      groups   = ["system:masters"]
      username = "gitlab-user"
      userarn  = aws_iam_user.user.arn
    }
  ]
  worker_groups = [
    {
      instance_type        = "t3a.medium"
      asg_max_size         = 4
      asg_desired_capacity = 3
      asg_min_size         = 2
    }
  ]
}
resource "kubernetes_namespace" "dev" {
  metadata {
    name = "dev"
    labels = {
      env = "dev"
    }
  }
  provisioner "local-exec" {
    command = "aws eks update-kubeconfig --name dev-eks --region us-east-1"
  }
}

resource "kubernetes_namespace" "staging" {
  metadata {
    name = "staging"
    labels = {
      env = "staging"
    }
  }
}

resource "kubernetes_namespace" "prod" {
  metadata {
    name = "prod"
    labels = {
      env = "prod"
    }
  }
}

output "eks-nodes-role" { value = module.eks.worker_iam_role_name }
output "eks-nodes-sg" { value = module.eks.worker_security_group_id }
output "eks-provider-arn" { value = module.eks.oidc_provider_arn }
