module "external_dns" {
  source = "git::https://github.com/DNXLabs/terraform-aws-eks-external-dns.git"

  cluster_name                     = local.cluster_name
  cluster_identity_oidc_issuer     = module.eks.cluster_oidc_issuer_url
  cluster_identity_oidc_issuer_arn = module.eks.oidc_provider_arn
  settings = {
    "policy" = "sync" # Modify how DNS records are sychronized between sources and providers.
  }
}
module "eks-iam-external-dns" {
  source       = "rhythmictech/eks-iam-external-dns/aws"
  version      = "0.1.0"
  cluster_name = local.cluster_name
  issuer_url   = module.eks.cluster_oidc_issuer_url
}
resource "aws_route53_zone" "cloudkite" {
  name = "cloudkite.kubeconfigstore.tk"

}
