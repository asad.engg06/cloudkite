resource "aws_iam_user" "user" {
  name = "gitlab"

  tags = {
    Name = "gitlab-user"
  }
}

resource "aws_iam_access_key" "key" {
  user = aws_iam_user.user.name
}

resource "aws_iam_user_policy" "lb_ro" {
  name = "ci"
  user = aws_iam_user.user.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
            "Effect": "Allow",
            "Action": [
                "eks:*"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": "iam:PassRole",
            "Resource": "*",
            "Condition": {
                "StringEquals": {
                    "iam:PassedToService": "eks.amazonaws.com"
            }
          }
  }  
]
}
EOF
}

resource "null_resource" "null-resource" {
  provisioner "local-exec" {
    command = "echo ${aws_iam_access_key.key.secret} > secret.txt"
  }
}

output "access_key" {
  value = aws_iam_access_key.key.id
}
