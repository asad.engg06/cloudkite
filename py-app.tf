resource "kubernetes_deployment" "py-app" {
  metadata {
    name      = "py-app"
    namespace = "dev"
    labels = {
      app = "py-app"
    }
  }

  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "py-app"
      }
    }
    template {
      metadata {
        labels = {
          app = "py-app"
        }
      }
      spec {
        container {
          image = "asadengg06/cloudkite:latest"
          name  = "py-app"
          resources {
            limits = {
              cpu    = "1"
              memory = "1Gi"
            }
            requests = {
              cpu    = "0.5"
              memory = "512Mi"
            }
          }
          port {
            container_port = 8080
          }
        }
      }
    }
  }
  depends_on = [kubernetes_namespace.dev]
}
resource "kubernetes_service" "py-app" {
  metadata {
    name      = "py-app-svc-dev"
    namespace = "dev"
    annotations = {
      "external-dns.alpha.kubernetes.io/hostname" = "cloudkite.kubeconfigstore.tk"
    }
  }
  spec {
    selector = {
      app = "py-app"
    }
    port {
      port        = 80
      target_port = 8080
    }

    type = "LoadBalancer"
  }
}
output "load_balancer_api-hostname" {
  value = kubernetes_service.py-app.status.0.load_balancer.0.ingress.0.hostname
}
